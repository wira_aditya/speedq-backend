<?php
	session_start();
	require_once 'config.php';

	$dataArr = array();

	if(isset($_POST['user']) && $_POST['user']!="")
	{
		$site = $_POST['user'];

		$query = "SELECT s._spotId, s._info, s._logo, s._namaSpot, s._initial, s._person FROM spot_ s WHERE s._siteId='$site' ORDER BY s._namaSpot ASC";
		$sql = $db->get_results($query);
		
		$i = 0;
		if ($sql) {
			foreach ($sql as $key => $value) {

				// Insert selected data to array
				$data['spotID'] = $value->_spotId;
				$data['namaSpot'] = $value->_namaSpot;
				$data['inisial'] = $value->_initial;
				$data['img'] = ($value->_logo=='http:%2F%2Fnull' || $value->_logo=='DEFAULT') ? $url."no.png" : $url.$value->_logo;
				$data['keterangan'] =  $value->_info;
				$data['penjaga'] =  $value->_person;
				
				$dataArr[$key] = $data;
				$i++;
			}
		}

		print_r(json_encode(
			array(
				"success"=>($sql) ? true : false,
				"message"=>($sql) ? "Load Data" : "Data Empty",
				"total"=>($sql) ? $i : 0,
				"data"=>$dataArr
			)
		));	
	}
	else
	{
		print_r(json_encode(
			array(
				"success"=>false,
				"message"=>"invalid parameter",
				"total"=>0,
				"data"=>$dataArr
			)
		));	
	}
?>
