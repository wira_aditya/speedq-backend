<?php
	session_start();
	require_once 'config.php';

	$dataArr = array();

	if(isset($_POST['user']) && $_POST['user']!="")
	{
		$site = $_POST['user'];

		$condition ="";

		$bulan = date('m');
		$tahun = date('Y');
		if(isset($_POST['bulan']) && $_POST['bulan']!="" && isset($_POST['tahun']) && $_POST['tahun']!="")
		{
			$bulan = $_POST['bulan'];
			$tahun = $_POST['tahun'];
		}

		if(isset($_POST['counter']) && $_POST['counter']!="")
		{
			$condition = "AND a._spotId='".$_POST['counter']."'";
		}

		
		$query = "SELECT DATE_FORMAT(a._tanggal,'%d-%m-%Y') as tgl, SUM(CASE WHEN a._statusAntrean = '1' OR a._statusAntrean = '2' OR a._statusAntrean = '3' OR a._statusAntrean = '6' THEN 1 ELSE 0 END) AS masuk, SUM(CASE WHEN a._statusAntrean >= '0' THEN 1 ELSE 0 END) AS daftar FROM antre_ a INNER JOIN spot_ s ON a._spotId=s._spotId WHERE MONTH(a._tanggal)='$bulan' AND YEAR(a._tanggal)='$tahun' AND s._siteId='$site' $condition GROUP BY a._tanggal";
		$sql = $db->get_results($query);
		
		$i = 0;
		$harga = 90;
		$jumlah = 0;
		if ($sql) {
			foreach ($sql as $key => $value) {

				// Insert selected data to array
				$data['tgl'] = $value->tgl;
				$data['daftar'] = $value->daftar;
				$data['check'] = $value->masuk;

				$jumlah = $jumlah+$value->masuk;

				$dataArr[$key] = $data;
				$i++;
			}
		}

		print_r(json_encode(
			array(
				"success"=>($sql) ? true : false,
				"message"=>($sql) ? "Load Data" : "Data Empty",
				"total"=>($sql) ? $i : 0,
				"tagihan"=>($sql) ? number_format($jumlah*$harga,0,",",".") : 0,
				"data"=>$dataArr
			)
		));	
	}
	else
	{
		print_r(json_encode(
			array(
				"success"=>false,
				"message"=>"invalid parameter",
				"total"=>0,
				"tagihan"=>0,
				"data"=>$dataArr
			)
		));	
	}
?>
