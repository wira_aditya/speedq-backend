<?php
	session_start();
	require_once 'config.php';

	$dataArr = array();

	if(isset($_POST['user']) && $_POST['user']!="")
	{
		$site = $_POST['user'];

		$tahun = date('Y');
		if(isset($_POST['tahun']) && $_POST['tahun']!="")
		{
			$tahun = $_POST['tahun'];
		}

		$ar_bul = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'];
		
		$query = "SELECT _billId, _bulan, _tahun, _invoice, _nominal, _status FROM bill_ WHERE _siteId='$site' AND YEAR(_tgl)='$tahun' ORDER BY _billId";
		
		$sql = $db->get_results($query);
		
		$i = 0;

		if ($sql) {
			foreach ($sql as $key => $value) {

				// Insert selected data to array
				$data['id'] = $value->_billId;
				$data['bulan'] = $value->_bulan;
				$data['bulanT'] = $ar_bul[$value->_bulan-1];
				$data['tahun'] = $value->_tahun;
				$data['invoice'] = $value->_invoice;
				$data['tagihan'] = number_format($value->_nominal,0,",",".");
				$data['status'] = $value->_status; //1=belum, 2=konfirmasi, 3=sudah
				$data['statusT'] = ($value->_status=="3") ? "Paid" : (($value->_status=="2") ? "Confirm" : "Unpaid");
				$dataArr[$key] = $data;
				$i++;
			}
		}

		print_r(json_encode(
			array(
				"success"=>($sql) ? true : false,
				"message"=>($sql) ? "Load Data" : "Data Empty",
				"total"=>($sql) ? $i : 0,
				"data"=>$dataArr
			)
		));	
	}
	else
	{
		print_r(json_encode(
			array(
				"success"=>false,
				"message"=>"invalid parameter",
				"total"=>0,
				"tagihan"=>0,
				"data"=>$dataArr
			)
		));	
	}
?>
