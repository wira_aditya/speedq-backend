<?php
	session_start();
	require_once 'config.php';

	$dataArr = array();

	if(isset($_POST['user']) && $_POST['user']!="")
	{
		$site = $_POST['user'];

		$tgl = date('Y-m-d');
		if(isset($_POST['tgl']) && $_POST['tgl']!="")
		{
			$tgl = $_POST['tgl'];
		}

		
		$query = "SELECT z._rating,z._ulasan,a._userId, a._spotId,a._code,a._antreId, a._nama as namaAn, a._keterangan, a._statusAntrean, s._namaSpot, s._initial, a._noAntrean, u._nama, u._kota,a._validasi, DATE_FORMAT(u._tglLahir, '%d-%m-%Y') as lahir 
								FROM antre_ a 
								LEFT JOIN speed_id.userdata_ u ON a._userId=u._UserID 
								left join rating_ z on a._antreId = z._antreId
								INNER JOIN spot_ s ON a._spotId=s._spotId  
								WHERE a._tanggal='".$tgl."' AND s._siteId='$site' 
								ORDER BY a._spotId, a._noAntrean ASC";
								
		$sql = $db->get_results($query);
		
		$i = 0;
		if ($sql) {
			foreach ($sql as $key => $value) {
				
				// Insert selected data to array
				$data['userID'] = $value->_userId;
				$data['_rating'] = $value->_rating == "" ? 0 : $value->_rating;
				$data['_ulasan'] = $value->_ulasan;
				

				$data['spotID'] = $value->_spotId;
				$data['namaSpot'] = $value->_namaSpot;
				$data['no'] = $value->_initial.$value->_noAntrean;
				$data['nama'] = ($value->_userId=="0") ? $value->namaAn : $value->_nama;
				$data['keterangan'] = $value->_keterangan;
				$data['kota'] = ($value->_userId=="0") ? "-" : $value->_kota;
				$data['lahir'] = ($value->_userId=="0") ? "-" : $value->lahir;
				$data['statusid'] = $value->_statusAntrean;
				$data['_validasi'] = $value->_validasi;
				$data['_antreId'] = $value->_antreId;
				$data['code'] = ($value->_code!='') ? $value->_code : '-';
				$data['status'] = ($value->_statusAntrean=="0") ? "Mendaftar" : (($value->_statusAntrean=="1") ? "Check-In" : (($value->_statusAntrean=="2") ? "Diproses" : (($value->_statusAntrean=="3") ? "Selesai" : (($value->_statusAntrean=="4") ? "Cancel" : (($value->_statusAntrean=="5") ? "Reject" : "Selesai")))));

				$dataArr[$key] = $data;
				$i++;
			}
		}

		print_r(json_encode(
			array(
				"success"=>($sql) ? true : false,
				"message"=>($sql) ? "Load Data" : "Data Empty",
				"total"=>($sql) ? $i : 0,
				"data"=>$dataArr
			)
		));	
	}
	else
	{
		print_r(json_encode(
			array(
				"success"=>false,
				"message"=>"invalid parameter",
				"total"=>0,
				"data"=>$dataArr
			)
		));	
	}
?>
