<?php
	session_start();
	require_once '../config.php';

	$data = array();

	if(isset($_POST['site']) && $_POST['site']!="")
	{
		$site = $_POST['site'];
		
		$query = "SELECT * FROM tv_ WHERE _siteId='$site' ORDER BY _tvId DESC";
		$sql = $db->get_results($query);
		$i = 1;
		if ($sql) {
			foreach ($sql as $key => $value) {
				$data['no'] = $i;
				$data['id_user'] = $value->_tvId;
				$data['username'] = $value->_username;

				$dataArr[$key] = $data;
				$i++;
			}

			print_r(json_encode(
				array(
					"success"=>true,
					"message"=>"Success get data",
					"data"=>$dataArr
				)
			));
		}
		else
		{
			print_r(json_encode(
				array(
					"success"=>false,
					"message"=>"Failed get data",
					"data"=>$dataArr
				)
			));
		}
	}
	else
	{
		print_r(json_encode(
			array(
				"success"=>false,
				"message"=>"invalid parameter",
				"data"=>$data
			)
		));	
	}
?>
