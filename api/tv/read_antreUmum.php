<?php
	session_start();
	require_once '../config.php';

	$dataArr = array();
	$dataDepan= array();
	$tgl = date('Y-m-d');

	if(isset($_POST['email']) && $_POST['email']!="")
	{
		$email = $_POST['email'];
		
		$query = "SELECT _spotId, _namaSpot, _initial FROM spot_ a
								inner join site_ b on a._siteId = b._siteId
								 WHERE b._emailSite='$email' AND a._statuson='1' ORDER BY _spotId ASC";
								//  print_r($query);
		$sql = $db->get_results($query);
		$jumlah_counter = (count($sql) > 6 ) ? 22 : 6;
		$i = 1;
		if ($sql) {

			foreach ($sql as $key => $value) {
				if($i<=$jumlah_counter)
				{	
					$data['no'] = $i;
					$data['spotId'] = $value->_spotId;
					$data['namaSpot'] = $value->_namaSpot;
					$data['_initial'] = "";
					$data['noAntre'] = 0;
					

					$noAnt = $db->get_row("SELECT _noAntrean FROM antre_ WHERE _spotId = '$value->_spotId' AND _tanggal='$tgl' AND (_statusAntrean='2' OR _statusAntrean='3' OR _statusAntrean='6') ORDER BY _noAntrean DESC LIMIT 1");
					if($noAnt){
						if($noAnt->_noAntrean > 0)
						{
							$data['noAntre'] = $value->_initial.$noAnt->_noAntrean;
						}						
					}

					$dataArr[$key] = $data;
				}

				$i++;
			}

			if(count($dataArr) < $jumlah_counter)
			{
				for($ii=count($dataArr); $ii <= $jumlah_counter; $ii++)
				{
					$data['no'] = $ii;
					$data['spotId'] = "";
					$data['namaSpot'] = "";
					$data['_initial'] = "";
					$data['noAntre'] = 0;

					$dataArr[$ii-1] = $data;
				}
			}

		}


		$queryDepan = "SELECT a._spotId, a._namaSpot, a._initial, b._noAntrean FROM spot_ a INNER JOIN antre_ b ON a._spotId=b._spotId WHERE date(updatep)='$tgl' ORDER BY updatep DESC LIMIT 1";
		$sqlDepan = $db->get_results($queryDepan);
		if ($sqlDepan) {
			foreach ($sqlDepan as $key => $valueDepan) {
				$data['no'] = 1;
				$data['spotId'] = $valueDepan->_spotId;
				$data['namaSpot'] = $valueDepan->_namaSpot;
				$data['_initial'] = $valueDepan->_initial;
				$data['noAntre'] = $valueDepan->_noAntrean;
				
				$dataDepan[$key] = $data;
			}
		}
		else
		{
			$data['no'] = 1;
			$data['spotId'] = "";
			$data['namaSpot'] = "";
			$data['_initial'] = "";
			$data['noAntre'] = "0";
			
			$dataDepan[0] = $data;
		}

		print_r(json_encode(
			array(
				"success"=>($sql) ? true : false,
				"message"=>($sql) ? "Load Data" : "Data Empty",
				"total"=>($sql) ? $i : 0,
				"data"=>$dataArr,
				"depan"=>$dataDepan
			)
		));	
	}
	else
	{
		print_r(json_encode(
			array(
				"success"=>false,
				"message"=>"invalid parameter",
				"total"=>0,
				"data"=>$dataArr,
				"depan"=>$dataDepan
			)
		));	
	}
?>
