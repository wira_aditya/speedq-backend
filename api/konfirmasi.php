<?php
	session_start();
	require_once 'config.php';

	$dataArr = array();

	if(isset($_POST['user']) && $_POST['user']!="" && isset($_POST['ed']) && $_POST['ed']!="")
	{
		$site = $_POST['user'];
		$id = $_POST['ed'];

		$checkKon = $db->get_var("SELECT COUNT(*) FROM bill_ WHERE _billId='$id' AND _siteId='$site' AND _status='2'");
		if($checkKon > 0)
		{
			print_r(json_encode(
				array(
					"success"=>false,
					"message"=>"You already confirm this Invoice",
					"data"=>$dataArr
				)
			));
		}
		else
		{
			$exeUP = $db->query("UPDATE bill_ SET _status = '2' WHERE _billId='$id' AND _siteId='$site'");
			
			print_r(json_encode(
				array(
					"success"=>($exeUP) ? true : false,
					"message"=>($exeUP) ? "Confirm Success" : "Confirm Faild",
					"data"=>$dataArr
				)
			));	
		}
	}
	else
	{
		print_r(json_encode(
			array(
				"success"=>false,
				"message"=>"invalid parameter",
				"total"=>0,
				"tagihan"=>0,
				"data"=>$dataArr
			)
		));	
	}
?>
