<?php
	session_start();
	require_once 'config.php';

	$dataArr = array();

	$query = "SELECT DATE_FORMAT(_tanggal, '%Y') as tahun FROM antre_ GROUP BY YEAR(_tanggal)";
	$sql = $db->get_results($query);
	
	$i = 0;
	
	if ($sql) {
		foreach ($sql as $key => $value) {

			// Insert selected data to array
			$data['tahun'] = $value->tahun;
			
			$dataArr[$key] = $data;
			$i++;
		}
	}

	print_r(json_encode(
		array(
			"success"=>($sql) ? true : false,
			"message"=>($sql) ? "Load Data" : "Data Empty",
			"total"=>($sql) ? $i : 0,
			"data"=>$dataArr
		)
	));	
?>
