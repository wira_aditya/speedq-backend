<?php
  require_once '../config.php';
  $id_antre = isset($_GET['id_antre']) && !empty($_GET['id_antre']) ? htmlspecialchars($_GET['id_antre']) : "";
  if(empty($id_antre)){
    print_r(
      json_encode(
        array(
          "success"=>false,
          "message"=>"Require Parameter"
        )
      )
      
    );
    die();
  }else{
    $qr = $db->get_row("SELECT 
                          a._code,a._estimasi,a._tanggal,a._noAntrean,b._initial,b._namaSpot,c._namaSite 
                          from antre_ a
                          inner join spot_ b on a._spotId = b._spotId
                          inner join site_ c on b._siteId = c._siteId
                          where _antreId = $id_antre
                          ");
    if(empty($qr)){
      print_r("404 NOT FOUND");
      die();
    }

  }
?>

<style>
  .site-name{
    font-size:20px;
  }
  
  .spot-name{
    font-size:15px;
  }
  .kotak{
    padding:20px;
    border:1px solid #000;
    border-radius:3px;
    text-align:center;
    font-family:sans-serif
  }
  p{
    margin:0;
  }
  .que-number-row{
    margin:12px 0px;
  }
  .number-que{
    font-size:35px;
    font-weight:bold;
  }
  .booking-code{
    font-weight:bold;
  }
  .text-foter-small{
    font-size:12px;
  }
</style>

<div class="kotak">
  <p class="site-name"><?= $qr->_namaSite ?></p>
  <p class="spot-name">(<?= $qr->_namaSpot ?>)</p>
  <div class="que-number-row">
    <p class="number-que">
      <?=
        $qr->_initial.
        $qr->_noAntrean
      ?>
    </p>
  </div>
  <div class="que-number-row">
    <p>Booking Code</p>
    <p class="booking-code"><?= $qr->_code ?></p>
  </div>
  <p>Date : <?= $qr->_tanggal ?></p>
  <!-- <p>Estimation : <?= $qr->_estimasi?></p>
  <p class="text-foter-small">(Estimation can change)</p> -->
</div>
<script>
window.print()
setTimeout(function(){ window.close() }, 500);
</script>