<?php
header("Content-type: application/vnd.ms-excel");
header("Content-Disposition: attachment; filename=InvoiceList.xls");
session_start();
?>
<style>
table{
	font-size:10px;
	border-collapse: collapse;
}
.total{
	padding: 10px;
	text-align: center;
}
.hyphenate {
  /* Careful, this breaks the word wherever it is without a hyphen */
  overflow-wrap: break-word;
  word-wrap: break-word;

  /* Adds a hyphen where the word breaks */
  -webkit-hyphens: auto;
  -ms-hyphens: auto;
  -moz-hyphens: auto;
  hyphens: auto;
}
.container {
  width: 80px;
  margin: auto auto 25px;
  padding: 5px;
}
</style>
<style type="text/css" media="print">
    @page { 
        size: landscape;
    }
</style>
<?php
require_once 'config.php';

$condition="";

$ar_bul = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'];

$site = $_GET['user'];

$tahun = date('Y');
if(isset($_POST['tahun']) && $_POST['tahun']!="")
{
	$tahun = $_POST['tahun'];
}

?>
	<h2><center> INVOICE LIST </center><h2>	
		<table width='100%' border='1'>
		
			<tr>
				<th>No</th>
				<th>Month</th>
				<th>Year</th>
				<th>Invoice</th>
				<th>Payment</th>
				<th>Status</th>
			</tr>
		<?php
			$querycetak = $db->get_results("SELECT _billId, _bulan, _tahun, _invoice, _nominal, _status FROM bill_ WHERE _siteId='$site' AND YEAR(_tgl)='$tahun' ORDER BY _billId");
		?>
		<?php
			$i = 1;
			if($querycetak){
				foreach($querycetak as $a){
					
		?>
		<tr>
			<td style='text-align:center;' width='5%'><?php echo $i; ?></td>
			<td width='15%'><?php echo $ar_bul[$a->_bulan-1]; ?></td>
			<td width='15%'><?php echo $a->_tahun; ?></td>
			<td width='15%'><?php echo $a->_invoice; ?></td>
			<td width='15%'><?php echo number_format($a->_nominal,0,",","."); ?></td>
			<td width='15%'><?php echo ($a->_status=="3") ? "Paid" : (($a->_status=="2") ? "Confirm" : "Unpaid"); ?></td>
		<?php
			$i++;
		}} ?>
	</table>