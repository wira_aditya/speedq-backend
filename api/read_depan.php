<?php
	session_start();
	require_once 'config.php';

	$dataArr = array();

	if(isset($_POST['user']) && $_POST['user']!="")
	{
		$cond='';
		$totalAtas=0;
		$totalBawah=0;
		$site = $_POST['user'];
		$month = date('m');
		$year = date('Y');

		$month = (isset($_POST['bulan']) && $_POST['bulan']!='') ? $_POST['bulan']: $month;
		$year = (isset($_POST['tahun']) && $_POST['tahun']!='') ? $_POST['tahun']: $year;
		$counter = (isset($_POST['counter']) && $_POST['counter']!='') ? $_POST['counter']: '';
		$status = (isset($_POST['status']) && $_POST['status']!='') ? $_POST['status']: '' ;

		$cond .= ($counter=='') ? ' AND s._siteId='.$site : 'AND a._spotId='.$counter;

		if($status!='')
		{
			if($status=='daftar')
			{
				$cond .= ' AND a._statusAntrean=0';
			}
			else
			{
				$cond .= ' AND a._statusAntrean='.$status;
			}
		}

		$list=array();
		$listBulanan=array();
		$arrbulan=['Jan', 'Feb', 'Mar', 'Apr', 'Mei', 'Jun', 'Jul', 'Augt', 'Sept', 'Okt', 'Nov', 'Des'];
		

		$start_date = "01-".$month."-".$year;
		$start_time = strtotime($start_date);

		$end_time = strtotime("+1 month", $start_time);
		$j = 0;
		$b = 0;
		for($i=$start_time; $i<$end_time; $i+=86400)
		{
			$tanggal=date('Y-m-d', $i);
			$query = "SELECT COUNT(a._antreId) as jum, IFNULL(COUNT(c._ratingid),0) as rating, IFNULL(SUM(c._rating),0) as nilairating FROM antre_ a INNER JOIN spot_ s ON a._spotId=s._spotId LEFT JOIN rating_ c ON a._antreId=c._antreId WHERE a._tanggal='".$tanggal."'".$cond;
			$sqlTanggal = $db->get_row($query);
			$data['tanggal'] = $tanggal;
			$data['antrean'] = $sqlTanggal->jum;
			$data['pemberi_rating'] = $sqlTanggal->rating;
			$data['rata'] = ($sqlTanggal->rating>0) ? $sqlTanggal->nilairating/$sqlTanggal->rating: '0';
			

			$list[$j] = $data;
			$totalAtas=$totalAtas+$sqlTanggal->jum;
		   	$j++;
		}

		for($b=0; $b<12; $i++)
		{
			$bulan=$b+1;
			$queryBulanan = "SELECT COUNT(a._antreId) as jum, IFNULL(COUNT(c._ratingid),0) as rating, IFNULL(SUM(c._rating),0) as nilairating FROM antre_ a INNER JOIN spot_ s ON a._spotId=s._spotId LEFT JOIN rating_ c ON a._antreId=c._antreId WHERE MONTH(a._tanggal)='".$bulan."' AND YEAR(a._tanggal)='".$year."'".$cond;
			$sqlBulanan = $db->get_row($queryBulanan);
			$dataBulanan['bulan'] = $bulan;
			$dataBulanan['text_bulan'] = $arrbulan[$b];
			$dataBulanan['antrean'] = $sqlBulanan->jum;
			$dataBulanan['pemberi_rating'] = $sqlBulanan->rating;
			$dataBulanan['rata'] = ($sqlBulanan->rating>0) ? $sqlBulanan->nilairating/$sqlBulanan->rating: '0';
			
			$listBulanan[$b] = $dataBulanan;
		   	
		   	$totalBawah=$totalBawah+$sqlBulanan->jum;
		   	$b++;
		}

		print_r(json_encode(
			array(
				"success"=>true,
				"message"=>"Load Data",
				"data_atas"=>$list,
				"data_bawah"=>$listBulanan,
				"totalAtas"=>$totalAtas,
				"totalBawah"=>$totalBawah
			)
		));	
	}
	else
	{
		print_r(json_encode(
			array(
				"success"=>false,
				"message"=>"invalid parameter",
				"data_atas"=>$list,
				"data_bawah"=>$listBulanan,
				"totalAtas"=>$totalAtas,
				"totalBawah"=>$totalBawah
			)
		));	
	}
?>
