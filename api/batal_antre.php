<?php
	require_once 'config.php';
  $id_antre = isset($_POST['id_antre']) && !empty($_POST['id_antre']) ? htmlspecialchars($_POST['id_antre']) : "";
  if(empty($id_antre)){
    print_r(
      json_encode(
        array(
          "success"=>false,
          "message"=>"Require Parameter"
        )
      )
    );
    die();
  }else{
    $qr = $db->query("UPDATE antre_ set _statusAntrean = 4 where _antreId = $id_antre");
    if($qr){
      print_r(
        json_encode(
          array(
            "success"=>true,
            "message"=>"Data Updated"
          )
        )
      );
    }else{
      print_r(
        json_encode(
          array(
            "success"=>false,
            "message"=>"Something error when update data"
          )
        )
      );
    }
  }
?>