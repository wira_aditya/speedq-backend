<?php
	session_start();
	require_once 'config.php';

	$dataArr = array();

	if(isset($_POST['user']) && $_POST['user']!="")
	{
		$cond='';
		$total=0;
		$site = $_POST['user'];
		$dari = $_POST['dari'];
		$sampai = $_POST['sampai'];
		$hari_ini = date('Y-m-d');

		$dari = (isset($_POST['dari']) && $_POST['dari']!='') ? $_POST['dari']: '';
		$sampai = (isset($_POST['sampai']) && $_POST['sampai']!='') ? $_POST['sampai']: '';
		$counter = (isset($_POST['counter']) && $_POST['counter']!='') ? $_POST['counter']: '';
		$status = (isset($_POST['status']) && $_POST['status']!='') ? $_POST['status']:'' ;

		if($dari!='' && $sampai!='')
		{
			$cond .=" AND (a._tanggal>='".$dari."' AND a._tanggal<='".$sampai."')"; 
		}
		else if($dari!='')
		{
			$cond .=" AND a._tanggal>='".$dari."'"; 
		}
		else if($sampai!='')
		{
			$cond .=" AND a._tanggal<='".$sampai."'"; 
		}
		else
		{
			$cond .=" AND a._tanggal='".$hari_ini."'"; 
		}

		$cond .= ($counter=='') ? ' AND s._siteId='.$site : 'AND a._spotId='.$counter;
		//$cond .= ($status!='') ? ' AND a._statusAntrean='.$status : '';

		if($status!='')
		{
			if($status=='daftar')
			{
				$cond .= ' AND a._statusAntrean=0';
			}
			else
			{
				$cond .= ' AND a._statusAntrean='.$status;
			}
		}

		$list=array();
		
		$query = "SELECT z._rating,z._ulasan,a._userId, a._spotId,a._antreId, a._nama as namaAn, a._keterangan, a._statusAntrean, s._namaSpot, s._initial, a._noAntrean, u._nama, u._kota,a._validasi, DATE_FORMAT(u._tglLahir, '%d-%m-%Y') as lahir 
								FROM antre_ a 
								LEFT JOIN speed_id.userdata_ u ON a._userId=u._UserID 
								left join rating_ z on a._antreId = z._antreId
								INNER JOIN spot_ s ON a._spotId=s._spotId  
								WHERE 1=1 ".$cond."
								ORDER BY a._spotId, a._noAntrean ASC";
		$sql = $db->get_results($query);
		
		$i = 0;
		$rating = 0;
		$nilairating = 0;
		if ($sql) {
			foreach ($sql as $key => $value) {

				$data['userID'] = $value->_userId;
				$data['_rating'] = $value->_rating == "" ? 0 : $value->_rating;
				$data['_ulasan'] = $value->_ulasan;
				

				$data['spotID'] = $value->_spotId;
				$data['namaSpot'] = $value->_namaSpot;
				$data['no'] = $value->_initial.$value->_noAntrean;
				$data['nama'] = ($value->_userId=="0") ? $value->namaAn : $value->_nama;
				$data['keterangan'] = $value->_keterangan;
				$data['kota'] = ($value->_userId=="0") ? "-" : $value->_kota;
				$data['lahir'] = ($value->_userId=="0") ? "-" : $value->lahir;
				$data['statusid'] = $value->_statusAntrean;
				$data['_validasi'] = $value->_validasi;
				$data['_antreId'] = $value->_antreId;
				$data['status'] = ($value->_statusAntrean=="0") ? "Mendaftar" : (($value->_statusAntrean=="1") ? "Check-In" : (($value->_statusAntrean=="2") ? "Diproses" : (($value->_statusAntrean=="3") ? "Selesai" : (($value->_statusAntrean=="4") ? "Cancel" : (($value->_statusAntrean=="5") ? "Reject" : "Selesai")))));

				if ($value->_rating != "") {
					$rating = $rating+1;
					$nilairating = $nilairating+$value->_rating;
				}
				$list[$key] = $data;
				$i++;
			}
		}



		print_r(json_encode(
			array(
				"success"=>($sql) ? true : false,
				"message"=>($sql) ? "Load Data" : "Data Empty",
				"total"=>($sql) ? $i : 0,
				"pemberi_rating"=>$rating,
				"rating"=>($rating>0) ? $nilairating/$rating : 0,
				"data"=>$list
			)
		));	
	}
	else
	{
		print_r(json_encode(
			array(
				"success"=>false,
				"message"=>"invalid parameter",
				"data"=>$list,
				"data"=>$list,
				"pemberi_rating"=>$rating,
				"rating"=>($rating>0) ? $nilairating/$rating : 0,
				"total"=>$i
			)
		));	
	}
?>
