<?php
	session_start();
	require_once 'config.php';

	$hari = date('N');
	$jam_kini = date('H:i');
	$time_kini = new DateTime($jam_kini);
	
	$dataArr = array();

	if(isset($_POST['user']) && $_POST['user']!="")
	{
		$site = $_POST['user'];

		$query = "SELECT s._allowOptDayChange as allowOptDayChange, s._spotId, s._info, s._logo, s._namaSpot, s._initial, s._person FROM spot_ s WHERE s._siteId='$site' ORDER BY s._namaSpot ASC";
		$sql = $db->get_results($query);
		
		$i = 0;
		$in = '0';
		if ($sql) {
			foreach ($sql as $key => $value) {
				
				$jam_buka = '';
				$jam_tutup = '';
				$since_start =0;
				// Insert selected data to array
				if($value->allowOptDayChange=='0')
	            {
	                //id_cari = [id_site, hari];
	                $queryHari= 'SELECT _jamBuka as jamBuka, _jamTutup as jamTutup FROM harioperasional_ WHERE _siteId='.$site.' AND _hari='.$hari.' AND _spotId=0';
	            }
	            else
	            {
	                //id_cari = [results[i].id, hari];
	                $queryHari= 'SELECT _jamBuka as jamBuka, _jamTutup as jamTutup FROM harioperasional_ WHERE _spotId='.$value->_spotId.' AND _hari='.$hari;
	            }
	            //$sql = $db->get_results($queryHari);
	            $dataQueryHari = $db->get_row($queryHari);
	            if($dataQueryHari)
	            {
	            	$jam_buka = $dataQueryHari->jamBuka;
	            	$jam_tutup = $dataQueryHari->jamTutup;
	            }
	            

				$time_buka = new DateTime($jam_buka);
				if($jam_buka < $time_kini)
				{
					$interval_buka =0;
				}
				else
				{
					$interval_buka = $time_buka->diff($time_kini);
					$interval_buka = $interval_buka->format('%h');
				}
				



				$time_tutup = new DateTime($jam_tutup);
				if($time_kini<$time_tutup)
				{
					$interval_tutup =0;
				}
				else
				{
					$interval_tutup = $time_kini->diff($time_tutup);
					$interval_tutup = $interval_tutup->format('%h');
				}
				
				
				if($interval_buka<3 && $interval_tutup<3)
				{
					$data['spotID'] = $value->_spotId;
					$data['spotID'] = $value->_spotId;
					$data['namaSpot'] = $value->_namaSpot;
					$data['inisial'] = $value->_initial;
					$data['img'] = ($value->_logo=='http:%2F%2Fnull' || $value->_logo=='DEFAULT') ? $url."no.png" : $url.$value->_logo;
					$data['keterangan'] =  $value->_info;
					$data['penjaga'] =  $value->_person;
					
					$dataArr[$in] = $data;
					$in++;
				 }
	            
				$i++;
			}
		}

		print_r(json_encode(
			array(
				"success"=>($sql) ? true : false,
				"message"=>($sql) ? "Load Data" : "Data Empty",
				"total"=>($sql) ? $in : 0,
				"data"=>$dataArr
			)
		));	
	}
	else
	{
		print_r(json_encode(
			array(
				"success"=>false,
				"message"=>"invalid parameter",
				"total"=>0,
				"data"=>$dataArr
			)
		));	
	}
?>
