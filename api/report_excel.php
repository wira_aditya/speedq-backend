<?php
header("Content-type: application/vnd.ms-excel");
header("Content-Disposition: attachment; filename=ReportQueue.xls");
session_start();
?>
<style>
table{
	font-size:10px;
	border-collapse: collapse;
}
.total{
	padding: 10px;
	text-align: center;
}
.hyphenate {
  /* Careful, this breaks the word wherever it is without a hyphen */
  overflow-wrap: break-word;
  word-wrap: break-word;

  /* Adds a hyphen where the word breaks */
  -webkit-hyphens: auto;
  -ms-hyphens: auto;
  -moz-hyphens: auto;
  hyphens: auto;
}
.container {
  width: 80px;
  margin: auto auto 25px;
  padding: 5px;
}
</style>
<style type="text/css" media="print">
    @page { 
        size: landscape;
    }
</style>
<?php
require_once 'config.php';
$site = $_GET['user'];

$condition ="";

$bulan = date('m');
$tahun = date('Y');
if(isset($_GET['bulan']) && $_GET['bulan']!="" && isset($_GET['tahun']) && $_GET['tahun']!="")
{
	$bulan = $_GET['bulan'];
	$tahun = $_GET['tahun'];
}

if(isset($_GET['counter']) && $_GET['counter']!="")
{
	$condition = "AND a._spotId='".$_GET['counter']."'";
}

?>
	<h2><center> REPORT QUEUE </center><h2>	
		<table width='100%' border='1'>
		
			<tr>
				<th>No</th>
				<th>Date</th>
				<th>Register</th>
				<th>Check In</th>
				
			</tr>
		<?php
			$querycetak = $db->get_results("SELECT DATE_FORMAT(a._tanggal,'%d-%m-%Y') as tgl, SUM(CASE WHEN a._statusAntrean = '1' OR a._statusAntrean = '2' OR a._statusAntrean = '3' OR a._statusAntrean = '6' THEN 1 ELSE 0 END) AS masuk, SUM(CASE WHEN a._statusAntrean >= '0' THEN 1 ELSE 0 END) AS daftar FROM antre_ a INNER JOIN spot_ s ON a._spotId=s._spotId WHERE MONTH(a._tanggal)='$bulan' AND YEAR(a._tanggal)='$tahun' AND s._siteId='$site' $condition GROUP BY a._tanggal");
		?>
		<?php
			$i = 1;
			if($querycetak){
				foreach($querycetak as $a){
					
		?>
		<tr>
			<td style='text-align:center;' width='5%'><?php echo $i; ?></td>
			<td width='15%'><?php echo $a->tgl; ?></td>
			<td width='15%'><?php echo $a->daftar; ?></td>
			<td width='15%'><?php echo $a->masuk; ?></td>
		<?php
			$i++;
		}} ?>
	</table>