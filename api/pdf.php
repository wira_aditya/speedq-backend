<?php
require_once("dompdf/dompdf_config.inc.php");
ob_start();
session_start();
?>
<style>
table{
	font-size:14px;
	border-collapse: collapse;
}

</style>
<style type="text/css" media="print">
    @page { 
        size: landscape;
    }
</style>
<?php
require_once 'config.php';


$ir = $_GET['id'];

$querycetak = $db->get_row("SELECT _billId, _bulan, _tahun, _invoice, _nominal, _status FROM bill_ WHERE _billId='$ir'");
$ar_bul = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'];

?>
<h2><center> INVOICE SPEED Q</center><h2>	
	<p>
		<b>Invoice Number :  <?php echo $querycetak->_invoice; ?></b>
		<br>
		<b>Month : <?php echo $ar_bul[$querycetak->_bulan-1]; ?></b>
		<br>
		<b>Year : <?php echo $querycetak->_tahun; ?></b>
	</p>	

	<p>Thank you for your trust in using our products. For your use in <?=$ar_bul[$querycetak->_bulan-1]." ".$querycetak->_tahun;?>, the following is the amount of the bill you have to pay.</p>
	<table width='100%' border='1'>
	
		<tr>
			<td colspan='2'>Use of <?=$ar_bul[$querycetak->_bulan-1]." ".$querycetak->_tahun;?> Subscriptions</td>
			<td><?=number_format($querycetak->_nominal,0,",",".");?></td> 
		</tr>
		
		<tr>
			<td></td>
			<td>Total</td>
			<td><b><?=number_format($querycetak->_nominal,0,",",".");?></b></td> 
		</tr>
		<tr>
			<td></td>
			<td>Tax(10%)</td>
			<td><b><?=number_format(($querycetak->_nominal/10),0,",",".");?></b></td> 
		</tr>
		<tr>
			<td></td>
			<td>Grand Total</td>
			<td><b><?=number_format((($querycetak->_nominal/10)+$querycetak->_nominal),0,",",".");?></b></td> 
		</tr>
	
	</table>
	<br>
	<p>We hope you can complete the invoice payment before passing the deadline given.</p>
	<br>
	<table width='100%' border='1'>
	
		<tr>
			<td><h1><center>BCA</center></h1></td>
		</tr>
		
		<tr>
			<td><center>7725088990<br><i>PT. Bamboomedia Cipta Persada</i></center></td>
		</tr>
	</table>
	<br>
	<p>Regards, <br>Speed Team</p>
<?php
$var = ob_get_clean();
$html = $var;


$dompdf = new DOMPDF();
$dompdf->load_html($html);
$dompdf->render();
$f;
$l;
if(headers_sent($f,$l))
{
    echo $f,'<br/>',$l,'<br/>';
    die('now detect line');
}
$dompdf->stream('Invoice.pdf');
$dompdf->output();
exit;
?>