<?php
	session_start();
	require_once 'config.php';
	include 'generate_noantre.php';
	

	$tgl = isset($_POST['tanggal']) ? $_POST['tanggal'] : "";
	$nama =  isset($_POST['nama']) ? $_POST['nama'] : "";
	$ket =  isset($_POST['keterangan']) ? $_POST['keterangan'] : "";
	$spot =  isset($_POST['spot']) ? $_POST['spot'] : "";
	$kiosk =  isset($_POST['kiosk']) ? $_POST['kiosk'] : ""; //nanti untuk menyesuaikan status pendaftaran, jika dari pendaftaran kirim 0
	$status = isset($_POST['status']) ? $_POST['status'] : "1"; //kalo 0=daftar

	//echo $spot;
	//die();


	$uniq_code = '';
	$timestamp = strtotime($tgl);
	$hari = date('w', $timestamp);
	if($hari==0){$hari="7";}
	$siteId='';
	$estimasi='00:00:00';

	$datakirim=array();
	$datakirim['nama']=$nama;
	$datakirim['spot']='';
	$datakirim['estimasi']='';
	$datakirim['no']='';




	//cek akun trial atau tidak
	$queryTrial = "SELECT COUNT(a._antreId) as antre, e._type as type FROM antre_ a INNER JOIN spot_ o ON a._spotId=o._spotId INNER JOIN site_ e ON o._siteId=e._siteId WHERE a._tanggal='$tgl' AND a._spotId='$spot'";
	$sqlTrial = $db->get_row($queryTrial);
	if ($sqlTrial) {

		if($sqlTrial->type==0 &&  $sqlTrial->antre>=15)
        {
            print_r(json_encode(
				array(
					"success"=>false,
					"message"=>"Sory your account is Trial and have limit"
				)
			));
        }
        else
        {
        	$querySama = "SELECT COUNT(*) as jumsama FROM antre_ WHERE _tanggal='$tgl' AND _spotId='$spot' AND _nama='$nama'";
			$sqlSama = $db->get_row($querySama);
			if($sqlSama->jumsama>0)
			{
				print_r(json_encode(
					array(
						"success"=>false,
						"message"=>"Sorry, this name is already in this counter"
					)
				));
			}
			else
	        {
	          	//CEK DULU apakah Antrean sudah penuh
				$queryPenuh = "SELECT s._batasAntrianSpot as batas, COUNT(a._noAntrean) as total_antre FROM antre_ a INNER JOIN spot_ s ON a._spotId=s._spotId WHERE a._tanggal='$tgl' AND a._spotId='$spot' AND (a._statusAntrean!=5 OR a._statusAntrean!=4)";
				$sqlPenuh = $db->get_row($queryPenuh);
				if ($sqlPenuh) {
					if($sqlPenuh->batas == 0 || ($sqlPenuh->batas != 0 && $sqlPenuh->batas>$sqlPenuh->total_antre))
					{
						//Cek dulu sudah antre apa belum
						$queryCekAntre = "SELECT COUNT(_antreId) as jum FROM antre_ WHERE _nama='$nama' AND _tanggal='$tgl' AND _spotId='$spot'";
						$sqlCekAntre = $db->get_row($queryCekAntre);
						if($sqlCekAntre && $sqlCekAntre->jum=="0")
						{
							//echo "sini1";
							$noAntrean=0;
	                        $allow='';
	                        $QueryOp='';
	                        $waktu='';
	                        $idAntre='';
							// tak tambah antre id(wira)
							//Ambil data Antrean
							$queryDataAntre = "SELECT b._namaSpot as namaSpot,a._antreId, b._initial as initial,b._siteId as siteId, b._allowOptDayChange as allow, b._estimasiWaktu as waktu, MAX(a._noAntrean) as antrian FROM antre_ a INNER JOIN spot_ b ON a._spotId=b._spotId WHERE a._tanggal='$tgl' AND a._spotId='$spot'";
							$sqlDataAntre = $db->get_row($queryDataAntre);
							if($sqlDataAntre)
							{
								$noAntrean = $sqlDataAntre->antrian+1;
	                            $allow = $sqlDataAntre->allow;
	                            $siteId=$sqlDataAntre->siteId;
	                            $waktu = $sqlDataAntre->waktu;
	                            $datakirim['spot']= $sqlDataAntre->namaSpot;
	                            $datakirim['no']= $sqlDataAntre->initial.$noAntrean;
	                            $datakirim['_antreId']= $sqlDataAntre->_antreId;
							}
							else
							{
								$noAntrean = 1;
	                            $allow = 0;
	                            $waktu = 0;
							}

							//Insert dulu

							if($kiosk=="0" && $status=='0')
							{
								$uniq_code = random($db);
								$datakirim['code']=$uniq_code;
								$queryInsert = $db->query("INSERT INTO antre_ (_nama, _statusAntrean, _userId, _spotId, _keterangan, _tanggal, _noAntrean, _hari, _code) VALUES ('$nama','$status','0','$spot','$ket','$tgl','$noAntrean','$hari', '$uniq_code')");
							}
							else
							{
								$queryInsert = $db->query("INSERT INTO antre_ (_nama, _statusAntrean, _userId, _spotId, _keterangan, _tanggal, _noAntrean, _hari) VALUES ('$nama','$status','0','$spot','$ket','$tgl','$noAntrean','$hari')");
							}
							
							$idbaru = $db->insert_id;
							$datakirim['_antreId_baru']=$idbaru;

							if($noAntrean==1)
							{
								//echo "ant1";
								//Ambil jumlah antrian
								$queryJumlahAntre = "SELECT COUNT(_noAntrean) as total_antre FROM antre_ WHERE _tanggal='$tgl' AND _spotId='$spot' AND (_statusAntrean!=6 OR _statusAntrean!=5 OR _statusAntrean!=4 OR _statusAntrean!=3) AND _antreId<$idbaru";
								$sqlJumlahAntre = $db->get_row($queryJumlahAntre);

								if($allow=="1"){
				                    $QueryOp="SELECT TIME_FORMAT(_jamBuka, '%H:%i') as buka FROM harioperasional_ WHERE _spotId = '$spot' AND _hari='$hari'";
				                }
				                else
				                {
				                    $QueryOp="SELECT TIME_FORMAT(_jamBuka, '%H:%i') as buka FROM harioperasional_ WHERE _siteId = '$siteId' AND _spotId=0 AND _hari='$hari'";
				                }
								
								//Cek Operasional
								$queryOperasional = $QueryOp;
								$sqlOperasional = $db->get_row($queryOperasional);
								if($db->num_rows > 0)
								{
									if(date('Y-m-d')<$tgl)
			                        {
			                          	//console.log('waktu '+waktu);
			                          
				                        $waktua = $waktu;
				                        $seconda = $waktua * 60;
				                        
				                        $waktub = explode(":",$sqlOperasional->buka);
				                        $secondb = ($waktub[0]) * 60 * 60 + (($waktub[1]) * 60);
				                      
				                        $newSecond = $secondb + ($seconda * ($sqlJumlahAntre->total_antre));

				                        $hours = floor($newSecond / 3600);
										$mins = floor($newSecond / 60 % 60);
										$secs = floor($newSecond % 60);

			                          	$estimasi = sprintf('%02d:%02d:%02d', $hours, $mins, $secs);
			                        }
			                        else
			                        {
			                            //jika jam buka sudah lewat atau sama dengan jam sekarang
			                            if($sqlJumlahAntre->total_antre==0 && ($sqlOperasional->buka<=date('H:i')))
			                            {
			                                $estimasi=date('H:i');
			                            }
			                            else if($sqlJumlahAntre->total_antre==0 && ($sqlOperasional->buka>date('H:i')))
			                            {
			                                $waktua = $waktu;
			                                $seconda = $waktua * 60;
			                                
			                                $waktub = explode(":",$sqlOperasional->buka);
			                                $secondb = ($waktub[0]) * 60 * 60 + (($waktub[1]) * 60);
			                        
			                                $newSecond = $secondb + ($seconda * ($sqlJumlahAntre->total_antre));
			                                
			                                $hours = floor($newSecond / 3600);
											$mins = floor($newSecond / 60 % 60);
											$secs = floor($newSecond % 60);

				                          	$estimasi = sprintf('%02d:%02d:%02d', $hours, $mins, $secs);
			                            }
			                            else
			                            {

			                                $waktua = $waktu;
			                                $seconda = $waktua * 60;
			                                
			                                $estimasi_akhir='00:00';
			                                $waktub = explode(":",$estimasi_akhir);
			                                $secondb = ($waktub[0]) * 60 * 60 + (($waktub[1]) * 60);
			                        
			                                $newSecond = $secondb + ($seconda);
			                                //var date = new Date(newSecond * 1000);
			                                $hours = floor($newSecond / 3600);
											$mins = floor($newSecond / 60 % 60);
											$secs = floor($newSecond % 60);

				                          	$estimasi = sprintf('%02d:%02d:%02d', $hours, $mins, $secs);

			                                //Kondisi jika jam estimasi lebih kecil dari jam sekarang
			                                if($estimasi<=date('H:i:s'))
			                                {
			                                    $estimasi = date('H:i:s');
			                                }
			                            }
			                        }

			                        //Update estimasi 
			                        $queryUpdate = $db->query("UPDATE antre_ SET _estimasi='$estimasi' WHERE _antreId='$idbaru'");
			                        if($queryUpdate)
			                        {
			                        	//echo "hasil1";
			                        	//$datakirim['estimasi']=$estimasi;
			                        	$datakirim['estimasi']=$estimasi;
			                        	print_r(json_encode(
											array(
												"success"=>true,
												"data"=>$datakirim,
												"message"=>"Success Add Antre"
											)
										));
			                        }
			                        else
			                        {
			                        	//echo "hasil1b";
			                        	print_r(json_encode(
											array(
												"success"=>false,
												"data"=>$datakirim,
												"message"=>"Sorry, Have Problem when save"
											)
										));
			                        }
			                        
								}
								else
								{
									$queryDelete = $db->query("DELETE FROM antre_ WHERE _antreId='$idbaru'");
									print_r(json_encode(
											array(
												"success"=>false,
												"data"=>$datakirim,
												"message"=>"Sorry, Have Problem when save"
											)
										));
								}
							}
							else
							{
								//echo "SELECT TIME_FORMAT(_jamBuka, '%H:%i') as buka FROM harioperasional_ WHERE _spotId = '$spot' AND _hari='$hari'";
								//estimasi maksimal yang ada
								$queryJamMax = "SELECT TIME_FORMAT(a._estimasi, '%H:%i') as maxes FROM antre_ a WHERE a._antreId <'$idbaru' AND a._tanggal='$tgl' AND a._spotId='$spot' AND (a._statusAntrean!=6 OR a._statusAntrean!=5 OR a._statusAntrean!=4 OR a._statusAntrean!=3) ORDER BY a._antreId DESC LIMIT 1";
								$sqlJamMax = $db->get_row($queryJamMax);

					            $estimasi_akhir = $sqlJamMax->maxes;

					            //Ambil Jumlah Antrian
					            $queryJumAnt = "SELECT COUNT(_noAntrean) as total_antre FROM antre_ WHERE _tanggal='$tgl' AND _spotId='$spot' AND (_statusAntrean!=6 OR _statusAntrean!=5 OR _statusAntrean!=4 OR _statusAntrean!=3) AND _antreId<'$idbaru'";
								$sqlJumAnt = $db->get_row($queryJumAnt);
					                 
					                 
					            if($allow=="1"){
					                $QueryOp="SELECT TIME_FORMAT(_jamBuka, '%H:%i') as buka FROM harioperasional_ WHERE _spotId = '$spot' AND _hari='$hari'";
					            }
					            else
					            {
					                $QueryOp="SELECT TIME_FORMAT(_jamBuka, '%H:%i') as buka FROM harioperasional_ WHERE _siteId = '$siteId' AND _spotId=0 AND _hari='$hari'";
					            }

					                    
					            //Cari Jam Buka dari Counter
					            $queryBukaCounter = $QueryOp;
								$sqlBukaCounter = $db->get_row($queryBukaCounter);
								if($db->num_rows > 0)
								{
									if(date('Y-m-d')<$tgl)
									{
										$waktua = $waktu;
					                    $seconda = $waktua * 60;
					                        
					                    $waktub =explode(":",$sqlBukaCounter->buka);
					                    $secondb = ($waktub[0]) * 60 * 60 + (($waktub[1]) * 60);
					                   
					                    $newSecond = $secondb + ($seconda * ($sqlJumAnt->total_antre));
					                    
					                    $hours = floor($newSecond / 3600);
										$mins = floor($newSecond / 60 % 60);
										$secs = floor($newSecond % 60);

				                        $estimasi = sprintf('%02d:%02d:%02d', $hours, $mins, $secs);
									}
									else
									{
										//jika jam buka sudah lewat atau sama dengan jam sekarang
					                    if($sqlJumAnt->total_antre==0 && $sqlBukaCounter->buka<=date('H:i'))
					                    {
					                        $estimasi=date('H:i');
					                    }
					                    else if($sqlJumAnt->total_antre==0 && $sqlBukaCounter->buka>date('H:i'))
					                    {
					                        $waktua = $waktu;
					                        $seconda = $waktua * 60;
					                        
					                        $waktub = explode(":",$sqlBukaCounter->buka);
					                        $secondb = ($waktub[0]) * 60 * 60 + ($waktub[1]) * 60;
					                    
					                        $newSecond = $secondb + ($seconda * ($sqlJumAnt->total_antre));
					                        
					                        $hours = floor($newSecond / 3600);
											$mins = floor($newSecond / 60 % 60);
											$secs = floor($newSecond % 60);

					                        $estimasi = sprintf('%02d:%02d:%02d', $hours, $mins, $secs);
					                    }
					                    else
					                    {

					                        $waktua = $waktu;
					                        $seconda = $waktua * 60;
					                        
					                        $waktub = explode(":",$estimasi_akhir);
					                        $secondb = ($waktub[0]) * 60 * 60 + ($waktub[1]) * 60;
					                    
					                        $newSecond = $secondb + ($seconda);

					                       	$hours = floor($newSecond / 3600);
											$mins = floor($newSecond / 60 % 60);
											$secs = floor($newSecond % 60);

					                        $estimasi = sprintf('%02d:%02d:%02d', $hours, $mins, $secs);

					                        //Kondisi jika jam estimasi lebih kecil dari jam sekarang
					                        if($estimasi<=date('H:i:s'))
					                        {
					                            $estimasi = date('H:i:s');
					                        }
					                    }
									}
									//Update estimasi 
			                        $queryUpdate = $db->query("UPDATE antre_ SET _estimasi='$estimasi' WHERE _antreId='$idbaru'");
			                        if($queryUpdate)
			                        {
			                        	//echo "hasil2";
			                        	$datakirim['estimasi']=$estimasi;
			                        	print_r(json_encode(
											array(
												"success"=>true,
												"data"=>$datakirim,
												"message"=>"Success Add Antre"
											)
										));
			                        }
			                        else
			                        {
			                        	//echo "hasil2b";
			                        	print_r(json_encode(
											array(
												"success"=>false,
												"message"=>"Sorry, Have Problem when save"
											)
										));
			                        }
								}
								else
								{
									$queryDelete = $db->query("DELETE FROM antre_ WHERE _antreId='$idbaru'");
									print_r(json_encode(
										array(
											"success"=>false,
											"data"=>$datakirim,
											"message"=>"Sorry, Have Problem when save"
										)
									));
								}
							}
						}
						else
						{
							print_r(json_encode(
								array(
									"success"=>false,
									"message"=>"Sorry, This name is already register"
								)
							));
						}
						
					}
					else
					{
						print_r(json_encode(
							array(
								"success"=>false,
								"message"=>"Sorry, Queue in this Counter is Full"
							)
						));
					}
				}
				else
				{
					print_r(json_encode(
						array(
							"success"=>false,
							"message"=>"Invaliq Queue"
						)
					));
				}
	        }
        }
        
		
	}
	else
	{
		print_r(json_encode(
			array(
				"success"=>false,
				"message"=>"invalid counter"
			)
		));
	}

/*
	$site="";
	$harga=90;

	$bulan = date('n');
	$tahun = date('Y');

	$tgl = date('Y-m-d');

	$noIn = date('YmHIs');

	if($bulan=="1")
	{
		$bulan = "12";
		$tahun = $tahun - 1;
	}
	else
	{
		$bulan = $bulan-1;
	}

	

	//cari yang sudah pakai 3 bulan/ 4 bulan karena 1 bulan penggunaan
	$querySite = "SELECT _siteId, _namaSite FROM site_ WHERE _type=1 AND DATE(_tanggalDaftar) <= DATE(NOW() - INTERVAL 4 MONTH)";
	$sqlSite = $db->get_results($querySite);
	if ($sqlSite) {
		foreach ($sqlSite as $key => $value) {

			$site = $value->_siteId;

			$queryJumlah = "SELECT COUNT(a._antreId) as jum FROM antre_ a INNER JOIN spot_ s ON a._spotId=s._spotId INNER JOIN site_ t ON s._siteId=t._siteId WHERE s._siteId='$site' AND (a._statusAntrean=1 OR a._statusAntrean=2 OR a._statusAntrean=3 OR a._statusAntrean=6) AND MONTH(a._tanggal)='$bulan' AND YEAR(a._tanggal)='$tahun'";

			$sqlJumlah = $db->get_row($queryJumlah);
			
			if($sqlJumlah->jum>15)
			{
				$angka=$sqlJumlah->jum-15;
				$tagihan = $angka*$harga;

				$query = $db->query("INSERT INTO bill_ (
						_siteId,
						_bulan,
						_tahun,
						_invoice,
						_nominal,
						_tgl
					) VALUES (
							$site,
							$bulan,
							'$tahun',
							'$noIn',
							$tagihan,
							'$tgl'
						)");

				
			}
		}
	}*/
?>
