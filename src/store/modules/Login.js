import axios from 'axios';

// action
const actions = {
  async getlogin({ context }, params) {
    try {
      const res = await axios.post(`${this.state.API_URL}/login/login.php`, params);
      return res.data;
    } catch (error) {
      return error;
    }
  },
  async getloginTv({ context }, params) {
    try {
      const res = await axios.post(`${this.state.API_URL}/tv/logintv.php`, params);
      return res.data;
    } catch (error) {
      return error;
    }
  },
};
const mutations = {};
const state = {};
export default {
  namespaced: true,
  actions,
  mutations,
  state,
};
