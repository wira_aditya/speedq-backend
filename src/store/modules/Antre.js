import axios from 'axios';

// state
const state = {
  dataAntre: [],
};
// action
const actions = {
  async getAntre({ commit },param) {
    try {
      const res = await axios.post(`${this.state.API_URL}/read_antre.php`,param);
      commit('setAntre', res.data.data);
    } catch (error) {
      console.error(error);
    }
  },
  async submitAntre({commit},param){
    try{
      const res =  await axios.post(`${this.state.API_URL}/add_antre.php`,param);
      return res.data;
    } catch (error) {
      console.error(error)
    }
  },
  async changeValidation({commit},param){
    try{
      const res =  await axios.post(`${this.state.API_URL}/validasi.php`,param);
      return res.data;
    } catch (error) {
      console.error(error)
    }
  },
  async cancelAntre({commit},param){
    try{
      const res =  await axios.post(`${this.state.API_URL}/batal_antre.php`,param);
      return res.data;
    } catch (error) {
      console.error(error)
    }
  }
};
const mutations = {
  setAntre(state, data) {
    state.dataAntre = data;
  },
};
export default {
  namespaced: true,
  state,
  actions,
  mutations,
  state,
};
