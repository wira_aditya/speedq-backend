import axios from 'axios';

// state
const state = {
  dataBill: [],
};
// action
const actions = {
  async getBill({ commit },param) {
    try {
      const res = await axios.post(`${this.state.API_URL}/read_bill.php`,param);
      commit('setBill', res.data.data);
    } catch (error) {
      console.error(error);
    }
  },
  async konfirmasi({commit},param){
    try {
      const res = await axios.post(`${this.state.API_URL}/konfirmasi.php`,param);
      return res.data;
    } catch (error) {
      console.error(error);
    }
  }
};
const mutations = {
  setBill(state, data) {
    state.dataBill = data;
  },
};
export default {
  namespaced: true,
  state,
  actions,
  mutations,
  state,
};
