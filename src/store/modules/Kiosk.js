import axios from 'axios';

// state
const state = {
};
// action
const actions = {
  async getBookingData({ commit },param) {
    try {
      const res = await axios.post(`${this.state.API_URL}/kiosk/checkBooking.php`,param);
      return res.data
    } catch (error) {
      console.error(error);
    }
  },
  
};
const mutations = {
  
};
export default {
  namespaced: true,
  state,
  actions,
  mutations,
  state,
};
