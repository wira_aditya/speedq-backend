import axios from 'axios';

// state
const state = {
  dataCounter: [],
};
// action
const actions = {
  async getCounter({ commit },param) {
    try {
      const res = await axios.post(`${this.state.API_URL}/read_counter.php`,param);
      commit('setCounter', res.data.data);
    } catch (error) {
      console.error(error);
    }
  },
  async getCounterKiosk({ commit },param) {
    
    try {
      const res = await axios.post(`${this.state.API_URL}/read_counter_kiosk.php`,param);
      return res.data
    } catch (error) {
      console.error(error);
    }
  },
};
const mutations = {
  setCounter(state, data) {
    state.dataCounter = data;
  },
};
export default {
  namespaced: true,
  state,
  actions,
  mutations,
  state,
};
