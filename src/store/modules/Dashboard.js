import axios from 'axios';

// state
const state = {
  dataTabelDashboard: [],
};
// action
const actions = {
  async getDataTabel({ commit },param) {
    try {
      const res = await axios.post(`${this.state.API_URL}/read_depan2.php`,param);
      commit('setData', res.data.data);
      return res.data;
    } catch (error) {
      console.error(error);
    }
  },
  async getDataChart({commit},param){
    try{
      const res =  await axios.post(`${this.state.API_URL}/read_depan.php`,param);
      return res.data;
    } catch (error) {
      console.error(error)
    }
  },
  
};
const mutations = {
  setData(state, data) {
    state.dataTabelDashboard = data;
  },
};
export default {
  namespaced: true,
  state,
  actions,
  mutations,
  state,
};
