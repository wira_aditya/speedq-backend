import axios from 'axios';

// state
const state = {
  cardDepan : {
    namaSpot: "",
    _initial: "",
    noAntre: "",
  },
  isVoice: false
};
// action
const actions = {
  setisVouce({commit}, param) {
    commit('setStateIsvoice',param);
  },
  async getImage({ commit },param) {
    try {
      const res = await axios.post(`${this.state.API_URL}/tv/read_foto.php`,param);
      return res.data
    } catch (error) {
      console.error(error);
    }
  },
  async doSetCardDepan({ commit }, param){
    commit('setCardDepan',param);
  },
  async getAntre({ commit },param) {
    try {
      const res = await axios.post(`${this.state.API_URL}/tv/read_antre.php`,param);
      return res.data
    } catch (error) {
      console.error(error);
    }
  },
  async getAntreUmum({ commit },param) {
    try {
      const res = await axios.post(`${this.state.API_URL}/tv/read_antreUmum.php`,param);
      return res.data
    } catch (error) {
      console.error(error);
    }
  },
  async setToken({ commit },param) {
    try {
      const res = await axios.post(`${this.state.API_URL}/tv/add_token.php`,param);
      return res.data
    } catch (error) {
      console.error(error);
    }
  },
  async getThemeUmum({ commit },param) {
    try {
      const res = await axios.post(`${this.state.API_URL}/tv/getTheme.php`,param);
      return res.data
    } catch (error) {
      console.error(error);
    }
  },
};
const mutations = {
  setCardDepan(state, data) {
    state.cardDepan.namaSpot = data.namaSpot;
    state.cardDepan._initial = data._initial;
    state.cardDepan.noAntre = data.noAntre;
  },
  setStateIsvoice(state, voicevalue) {
    state.isVoice = voicevalue;
  }
};
export default {
  namespaced: true,
  state,
  actions,
  mutations,
  state,
};
