import axios from 'axios';

// state
const state = {
  dataTahun: [],
};
// action
const actions = {
  async getTahun({ commit },param) {
    try {
      const res = await axios.post(`${this.state.API_URL}/read_tahun.php`,param);
      commit('setTahun', res.data.data);
    } catch (error) {
      console.error(error);
    }
  },
};
const mutations = {
  setTahun(state, data) {
    state.dataTahun = data;
  },
};
export default {
  namespaced: true,
  state,
  actions,
  mutations,
  state,
};
