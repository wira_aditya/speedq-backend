import axios from 'axios';

// state
const state = {
  dataReport: [],
};
// action
const actions = {
  async getReport({ commit },param) {
    try {
      const res = await axios.post(`${this.state.API_URL}/read_report.php`,param);
      commit('setReport', res.data);
    } catch (error) {
      console.error(error);
    }
  },
};
const mutations = {
  setReport(state, data) {
    state.dataReport = data;
  },
};
export default {
  namespaced: true,
  state,
  actions,
  mutations,
  state,
};
