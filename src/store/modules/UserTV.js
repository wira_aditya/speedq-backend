import axios from 'axios';

// state
const state = {
  dataUser: [],
};
// action
const actions = {
  async getUser({ commit },param) {
    try {
      const res = await axios.post(`${this.state.API_URL}/tv/read_user.php`,param);
      commit('setUser', res.data.data);
    } catch (error) {
      console.error(error);
    }
  },
  async submitUser({commit},param){
    try{
      const res =  await axios.post(`${this.state.API_URL}/tv/tambah_user.php`,param);
      return res.data;
    } catch (error) {
      console.error(error)
    }
  },
  async updateUser({commit},param){
    try{
      const res =  await axios.post(`${this.state.API_URL}/tv/edit_user.php`,param);
      return res.data;
    } catch (error) {
      console.error(error)
    }
  },
  async deleteUser({commit},param){
    try{
      const res =  await axios.post(`${this.state.API_URL}/tv/delete_user.php`,param);
      return res.data;
    } catch (error) {
      console.error(error)
    }
  },
};
const mutations = {
  setUser(state, data) {
    state.dataUser = data;
  },
};
export default {
  namespaced: true,
  state,
  actions,
  mutations,
  state,
};
