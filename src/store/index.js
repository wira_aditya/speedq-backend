import Vue from 'vue';
import Vuex from 'vuex';

import login from './modules/Login';
import antre from './modules/Antre';
import counter from './modules/Counter';
import tahun from './modules/Tahun';
import report from './modules/Report';
import bill from './modules/Bill';
import monitor from './modules/Monitor';
import kiosk from './modules/Kiosk';
import dashboard from './modules/Dashboard';
import usertv from './modules/UserTV';

Vue.use(Vuex);

const store = new Vuex.Store({
  modules: {
    login,
    antre,
    counter,
    report,
    tahun,
    bill,
    monitor,
    kiosk,
    dashboard,
    usertv,
  },
  state: {
    // API_URL: (process.env.NODE_ENV === 'production') ? 'http://192.168.0.108/speedq-backend/api' : 'http://192.168.0.108/speedq-backend/api',
    API_URL: (process.env.NODE_ENV === 'production') ? 'https://clientq-beta.isn-speed.com/api' : 'http://localhost/speedq-backend/api',
    // API_URL: (process.env.NODE_ENV === 'production') ? 'https://clientq.isn-speed.com/api' : 'http://localhost/speedq-backend/api',
    success_flag: false,
  },
  mutations: {
  },
  actions: {
  },
});

export default store;
