import Vue from 'vue';
import VueCookies from 'vue-cookies';
import App from './App.vue';
import router from './router';
import store from './store';
import VueSweetalert2 from 'vue-sweetalert2';
import './registerServiceWorker';
import * as uiv from 'uiv';
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap-vue/dist/bootstrap-vue.css';

import '@fortawesome/fontawesome-free/css/all.css';
import VueSweetAlert from 'vue-sweetalert'
import HighchartsVue from 'highcharts-vue'

Vue.use(HighchartsVue)
Vue.use(VueSweetAlert)

Vue.config.productionTip = false;
Vue.use(uiv);
Vue.use(VueCookies);
Vue.use(VueSweetalert2);
new Vue({
  router,
  store,
  render: h => h(App),
}).$mount('#app');