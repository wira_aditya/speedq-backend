import Vue from 'vue';
import Router from 'vue-router';
import Home from './views/Home.vue';

Vue.use(Router);

export default new Router({
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home,
    },
    {
      path: '/login',
      name: 'login',
      component: () => import(/* webpackChunkName: "login" */ './views/Login.vue'),

    },
    {
      path: '/antrian',
      name: 'antrian',
      component: () => import(/* webpackChunkName: "about" */ './views/Antrian.vue'),
    },
    {
      path: '/report',
      name: 'report',
      component: () => import(/* webpackChunkName: "about" */ './views/Report.vue'),
    },
    {
      path: '/billing',
      name: 'billing',
      component: () => import(/* webpackChunkName: "about" */ './views/Billing.vue'),
    },
    {
      path: '/monitor',
      name: 'monitor',
      component: () => import(/* webpackChunkName: "about" */ './views/Monitor.vue'),
    },
    {
      path: '/monitor-umum/:id',
      name: 'monitor_umum',
      component: () => import(/* webpackChunkName: "about" */ './views/monitor_umum.vue'),
    },
    {
      path: '/kiosk',
      name: 'kiosk',
      component: () => import(/* webpackChunkName: "about" */ './views/kiosk.vue'),
    },
    {
      path: '/dashboard',
      name: 'dashboard',
      component: () => import(/* webpackChunkName: "about" */ './views/dashboardReport.vue'),
    },
    {
      path: '/user-tv',
      name: 'usertv',
      component: () => import(/* webpackChunkName: "about" */ './views/UserTv.vue'),
    }
  ],
});
